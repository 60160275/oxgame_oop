public class Player {
	private char name;
	private int win;
	private int lose;
	private int draw;

	public Player() {
		super();
	}

	public Player(char name) {
		super();
		this.name = name;
	}

	public char getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public int getLose() {
		return lose;
	}

	public int getDraw() {
		return draw;
	}

	public void win() {
		win++;
	}

	public void lose() {
		lose++;
	}

	public void draw() {
		draw++;
	}

}
