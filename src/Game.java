
import java.util.Scanner;

public class Game {
	private Board board;
	private Player x;
	private Player o;

	public Game() {
		o = new Player('o');
		x = new Player('x');

	}

	public void play() {
		showWelcome();
		while (true) {
			playOne();
		}
	}

	public void playOne() {

		board = new Board(x, o);
		while (true) {
			showTable();
			showTurn();
			input();
			if (board.isFinish()) {
				break;
			}
			board.switchPlayer();

		}
		showTable();
		if (board.hasWinner()) {

			showWinner();
		}
		showStat();
	}

	private void showStat() {

		System.out.println(x.getName() + " W/D/L : " + x.getWin() + "/" + x.getDraw() + "/" + x.getLose());
		System.out.println(o.getName() + " W/D/L : " + o.getWin() + "/" + o.getDraw() + "/" + o.getLose());

	}

	private void showWinner() {
		Player player = board.getWinner();
		System.out.println(player.getName() + " win....");

	}

	private void showWelcome() {
		System.out.println("Welcome to OX Game.");

	}

	private void showTable() {
		char[][] table = board.getTable();
		System.out.println("  1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(" " + table[i][j]);

			}
			System.out.println();
		}

	}

	private void showTurn() {
		/*
		 * Player player = board.getCurrentPlayer();
		 * System.out.println(player.getName()+" turn...");
		 */
		System.out.println(board.getCurrentPlayer().getName() + " turn...");

	}

	private void input() {
		Scanner kb = new Scanner(System.in);
		while (true) {
			try {
				System.out.print("Please input Row Col: ");
				String input = kb.nextLine();
				String[] str = input.split(" ");
				if (str.length != 2) {
					System.out.println("Please input Rol Col[1-3] (ex: 1 2) ");
					continue;

				}
				int row = Integer.parseInt(str[0]) - 1;
				int col = Integer.parseInt(str[1]) - 1;
				if (board.setTable(row, col) == false) {
					System.out.println("Table is not empty!!");
					continue;
				}
				break;
			} catch (Exception e) {
				System.out.println("Please input Rol Col[1-3] (ex: 1 2) ");
				continue;
			}

		}

	}

}
